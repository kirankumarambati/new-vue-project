<?php 

function custom_copy($src, $dst) { 

	// open the source directory 
	$dir = opendir($src); 

	// Make the destination directory if not exist 
	@mkdir($dst); 

	// Loop through the files in source directory 
	while( $file = readdir($dir) ) {
		
		echo $file;
		
		if($file == 'package' || $file == '.git' || $file == 'scripts.php') {
			
		} else if (( $file != '.' ) && ( $file != '..' )) { 
			if ( is_dir($src . '/' . $file) ) 
			{ 

				// Recursively calling custom copy function 
				// for sub directory 
				custom_copy($src . '/' . $file, $dst . '/' . $file); 

			} 
			else { 
				copy($src . '/' . $file, $dst . '/' . $file); 
			} 
		} 
	} 

	closedir($dir);
}

function addFolderToZip($pathdir, $destination)
{
	$newzip = new ZipArchive;
   if($newzip -> open($destination, ZipArchive::CREATE ) === TRUE) {
	  $dir = opendir($pathdir);
	  while($file = readdir($dir)) {
		if(is_file($pathdir.$file)) {
			$newzip -> addFile($pathdir.$file, $file);
		 }
	  }
	  $newzip ->close();
	  
	  /* $all= new RecursiveIteratorIterator(new RecursiveDirectoryIterator($pathdir));

		foreach ($all as $f=>$value) {
			$newzip->addFile(realpath($f), $f) or die ("ERROR: Unable to add file: $f");
		}
		$newzip ->close(); */
   }
   
   
}


$src = "./"; 

$dst = "./package";

custom_copy($src, $dst);

// $pathdir = "D:/Work/jfrog/new-vue-project/package/";
// $zipFolderName = "package.zip";
// addFolderToZip($pathdir, $zipFolderName);
?> 